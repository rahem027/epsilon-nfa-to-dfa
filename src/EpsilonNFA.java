import java.util.*;

public class EpsilonNFA {
    final List<String> states;
    final String sigma;
    final HashMap<String, HashMap<Character, List<String>>> delta;
    final String initial_state;
    final List<String> final_states;


    public EpsilonNFA(List<String> states, String sigma, HashMap<String, HashMap<Character, List<String>>> delta, String initial_state, List<String> final_states) {
        this.states = states;
        this.sigma = sigma;
        this.delta = delta;
        this.initial_state = initial_state;
        this.final_states = final_states;
    }

    Set<String> epsilonClosure(Set<String> states) {
        Set<String> result = new HashSet<>();

        for (String state : states) {
            result.addAll(epsilonClosure(state));
        }

        return result;
    }

    Set<String> epsilonClosure(String state) {
        return epsilonClosureOtherThan(state, new HashSet<>());
    }

    /**
     If transition table is such that there are loops in epislon
     transitions, than a naive recursive approach overflows the stack

     The following delta will overflow the stack

            1    ε
     -------------
     q0 |   |   q1
     q1 |   |   q0

     The way to resolve this is to pass in the computed closure in the method.
     If the closure has the current state, it means the closure was already
     computed. We should just return the existing closure instead of computing
     closure again.
    */
    private Set<String> epsilonClosureOtherThan(String state, Set<String> closure) {
        if (!closure.add(state)) {
            return closure;
        }

        for (String possibleOutput : delta.get(state).get('ε')) {
            closure.addAll(epsilonClosureOtherThan(possibleOutput, closure));
        }

        return closure;
    }

    Set<String> transitions(Set<String> states, char input) {
        Set<String> result = new HashSet<>();

        for (String state : states) {
            result.addAll(delta.get(state).get(input));
        }

        return result;
    }

    EpsilonNFA asNFA() {
        HashMap<String, HashMap<Character, List<String>>> new_delta = new HashMap<>();

        for (String state : states) {
            new_delta.put(state, new HashMap<>());
            Set<String> closure = epsilonClosure(state);

            for (char input : sigma.toCharArray()) {
                ArrayList<String> final_transitions = new ArrayList<>(
                        epsilonClosure(transitions(closure, input))
                );
                final_transitions.sort(String::compareTo);
                new_delta.get(state).put(input, final_transitions);
            }
        }

        return new EpsilonNFA(
                new ArrayList<>(states),
                sigma.substring(0, sigma.length() - 1),
                new_delta,
                initial_state,
                final_states
        );
    }

    void pretty_print() {
        ArrayList<Integer> possible_width = new ArrayList<>();
        for (var state_entry : delta.entrySet()) {
            String state = state_entry.getKey();
            possible_width.add(state.length());

            for (var transition_entry : state_entry.getValue().entrySet()) {
                int width = 0;
                List<String> states = transition_entry.getValue();
                for (String s : states) {
                    width += s.length();
                }

                /// 2 for arrow for initial state ->, and 2 for
                /// parenthesis for final state ()
                width += 4;
                possible_width.add(width);
            }
        }

        final int width_of_cell = possible_width.stream().max(Integer::compare).orElseThrow();
        System.out.println("Width: " + width_of_cell);

        /// One empty space and one space to account for separator
        /// in other rows
        System.out.printf("%" + width_of_cell + "s  ", "");

        for (char input : sigma.toCharArray()) {
            System.out.printf("%" + width_of_cell + "s |", input);
        }

        System.out.println();
        horizontal_line(width_of_cell);

        for (String state : states) {
            String state_to_display = state;

            boolean is_final = final_states.contains(state);
            boolean is_initial = Objects.equals(state, initial_state);

            if (is_final && !is_initial) {
                state_to_display = "  (" + state_to_display + ")";
            } else if (is_initial && !is_final) {
                state_to_display = "-> " + state_to_display + " ";
            } else if (is_initial) {
                state_to_display = "->(" + state_to_display + ")";
            } else {
                state_to_display = " " + state_to_display + " ";
            }

            System.out.printf("%" + width_of_cell + "s |", state_to_display);

            for (char input : sigma.toCharArray()) {
                System.out.printf("%" + width_of_cell + "s |", String.join(" ", delta.get(state).get(input)));
            }

            System.out.println();
        }

        horizontal_line(width_of_cell);
    }

    private void horizontal_line(int width) {
        /// Each cell has width spaces. We have one extra column
        /// to print and one extra space at end of each cell.
        /// So (width + 1) * (sigma.size() + 1)
        /// We also have sigma.size() + 1 separators i.e. "|"
        System.out.println("-".repeat((width + 1) * (sigma.length() + 1) + sigma.length() + 1));
    }
}
