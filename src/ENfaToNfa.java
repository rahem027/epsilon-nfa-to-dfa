import java.util.*;

public class ENfaToNfa {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the sigma without spaces");
        String sigma = in.next();

        sigma += "ε";

        System.out.println("Sigma: " + sigma);

        System.out.println("enter the number of states");
        int number_of_states = in.nextInt();

        ArrayList<String> states = new ArrayList<>();
        ArrayList<String> final_states = new ArrayList<>();

        for (int i = 0; i < number_of_states; i++) {
            System.out.println("Enter the state");
            String state = in.next();
            states.add(state);

            System.out.println("Is this state final");
            if (in.nextBoolean()) {
                final_states.add(state);
            }
        }

        states.sort(String::compareTo);
        System.out.println("states: ");

        for (String state : states) {
            System.out.println(state);
        }

        final_states.sort(String::compareTo);
        System.out.println("final states: ");
        for (String final_state : final_states) {
            System.out.println(final_state);
        }

        System.out.println("enter the initial state");
        String initial_state = in.next();
        System.out.println(initial_state);

        in.nextLine();

        HashMap<String, HashMap<Character, List<String>>> delta = new HashMap<>();

        for (String state : states) {
            delta.put(state, new HashMap<>());
            for (char input : sigma.toCharArray()) {
                delta.get(state).put(input, new ArrayList<>());
            }
        }

        for (String state : states) {
            for (char input : sigma.toCharArray()) {
                System.out.println(
                        "where will " + state
                                + " go on " + input
                );

                System.out.println("Enter -1 for no transition");

                String s = in.nextLine();

                if (Objects.equals(s, "-1")) {
                    continue;
                }

                List<String> transitions = Arrays.asList(s.split(" "));
                transitions.sort(String::compareTo);
                delta.get(state).put(input, transitions);
            }
        }

        EpsilonNFA enfa = new EpsilonNFA(states, sigma, delta, initial_state, final_states);
        enfa.pretty_print();
        System.out.println("NFA: ");
        EpsilonNFA nfa = enfa.asNFA();
        nfa.pretty_print();
    }
}
